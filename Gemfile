# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby File.read('.ruby-version').strip

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 6.1.4'
# Use Puma as the app server
gem 'puma', '~> 5.5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

gem 'pg', '~> 1.0'

gem 'jwt'

gem 'activerecord-import'

gem 'active_model_serializers'

gem 'rack-cors', require: 'rack/cors'

gem 'rollbar'

gem 'skylight'

gem 'sidekiq', '~> 6.2.2'
# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

gem 'strip_attributes'

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
# gem 'rack-cors'

group :development, :test do
  gem 'pry'
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'capybara'
  gem 'database_cleaner'
  gem 'dotenv-rails'
  gem 'factory_bot_rails'
  gem 'faker'
  gem 'rspec'
  gem 'rspec-rails'
  gem 'rubocop', require: false
  gem 'rubocop-performance'
  gem 'rubocop-rails'
  gem 'rubocop-rspec', require: false
  gem 'simplecov', require: false

  # audit libs
  # gem 'inquisition', github: 'rubygarage/inquisition'
end

group :development do
  gem 'brakeman'
  gem 'bullet'
  gem 'bundle-audit'
  gem 'dawnscanner'
  gem 'fasterer'
  gem 'listen', '>= 3.0.5', '<= 3.7.0'
  gem 'rails_best_practices'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  # audit libs
  gem 'active_record_doctor'
  gem 'lol_dba'
end

group :test do
  gem 'rspec_junit_formatter'
  gem 'shoulda-matchers'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data'
